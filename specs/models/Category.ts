import { Entity, Column, OneToMany, PrimaryGeneratedColumn } from "typeorm/index";
import { Product } from "./Product";
import { Model } from "../../src";


@Entity({name: "Categories"})
export class Category extends Model
{
    @PrimaryGeneratedColumn()
    public readonly id: number;

    @Column()
    public name: string;

    @OneToMany(type => Product, product => product.category, {cascade: true, lazy: true})
    public products: Promise<Product[]>;

}
