import { Entity, Column, PrimaryGeneratedColumn } from "typeorm/index";
import { Model } from "../../src"


export type Sex = "male" | "female";

@Entity({name: "Users"})
export class User extends Model
{
    @PrimaryGeneratedColumn()
    public readonly id: number;

    @Column()
    public name: string;

    @Column()
    public sex: Sex;

    @Column({type: "int"})
    public age: number;

    @Column({type: "int"})
    public weight: number
}
