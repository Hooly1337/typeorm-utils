import { Entity, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn } from "typeorm/index";
import { Category } from "./Category";
import { Model } from "../../src";


@Entity({name: "Products"})
export class Product extends Model{

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column({type: "int"})
    public categoryId: number;

    @ManyToOne(type => Category)
    @JoinColumn({name: "categoryId"})
    public category: Category;
}
