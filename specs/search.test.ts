import { createConnection, getConnection, getRepository } from "typeorm/index";
import { Op } from "../src";
import { User } from "./models"



describe("Testing search util", () =>
{
    beforeAll(async () =>
    {
        const conn = await createConnection({
            type: "sqlite",
            database: ":memory:",
            dropSchema: true,
            entities: [User],
            synchronize: true,
            logging: false
        });
        const rep = await getRepository(User);

        await rep.insert(
            {
                name: "John",
                age: 21,
                sex: "male",
                weight: 71
            });

        await rep.insert(
            {
                name: "Marry",
                age: 25,
                sex: "female",
                weight: 56
            });

        await rep.insert(
            {
                name: "Alex",
                age: 19,
                sex: "male",
                weight: 65
            });

        await rep.insert(
            {
                name: "Luci",
                age: 16,
                sex: "female",
                weight: 45
            });

        await rep.insert(
            {
                name: "Oleg",
                age: 15,
                sex: "male",
                weight: 54
            });
    });


    afterAll(() =>
    {
        let conn = getConnection();
        return conn.close();
    });


    it("should find male 18+", async () =>
    {
        let serveTheMotherland = await User.search({
            where: {
                age: Op.gte(18),
                sex: Op.eq("male")
            }
        });

        expect(JSON.stringify(serveTheMotherland)).toBe(JSON.stringify([
            {
                id: 1,
                name: "John",
                sex: "male",
                age: 21,
                weight: 71
            },
            {
                id: 3,
                name: "Alex",
                sex: "male",
                age: 19,
                weight: 65
            }
        ]));
    });


    it("should find all people", async () =>
    {
        let people = await User.search({
            where: {
                sex: "female",
                [Op.OR]: {
                    sex: "male"
                }
            }
        });

        expect(JSON.stringify(people)).toBe(JSON.stringify([
            {
                id: 1,
                name: "John",
                sex: "male",
                age: 21,
                weight: 71
            },
            {
                id: 2,
                name: "Marry",
                sex: "female",
                age: 25,
                weight: 56
            },
            {
                id: 3,
                name: "Alex",
                sex: "male",
                age: 19,
                weight: 65
            },
            {
                id: 4,
                name: "Luci",
                sex: "female",
                age: 16,
                weight: 45
            },
            {
                id: 5,
                name: "Oleg",
                sex: "male",
                age: 15,
                weight: 54
            }
        ]));
    });


    it("should find ids 1 and 3", async () =>
    {
        let result = await User.search({
            where: {
                id: 1,
                [Op.OR] : {
                    id: 3
                }
            }
        });

        expect(JSON.stringify(result)).toBe(JSON.stringify([
            {
                id: 1,
                name: "John",
                sex: "male",
                age: 21,
                weight: 71
            },
            {
                id: 3,
                name: "Alex",
                sex: "male",
                age: 19,
                weight: 65
            }
        ]));
    });

    it("should find all", async () =>
    {
        let result = await User.search({
            where: {}
        });

        expect(JSON.stringify(result)).toBe(JSON.stringify([
            {
                id: 1,
                name: "John",
                sex: "male",
                age: 21,
                weight: 71
            },
            {
                id: 2,
                name: "Marry",
                sex: "female",
                age: 25,
                weight: 56
            },
            {
                id: 3,
                name: "Alex",
                sex: "male",
                age: 19,
                weight: 65
            },
            {
                id: 4,
                name: "Luci",
                sex: "female",
                age: 16,
                weight: 45
            },
            {
                id: 5,
                name: "Oleg",
                sex: "male",
                age: 15,
                weight: 54
            }
        ]));
    });

    it("should throw error, because params are wrong", async done =>
    {
        try
        {
            let result = await User.search({
                where: {
                    id: 1,
                    [Op.OR]: []
                }
            });
            done.fail(`Expected error, but there"s nothing`)
        }
        catch (e)
        {
            // Wrong Condition
            done()
        }


    });

    it("should find all users 10-19 y.o.", async () =>
    {
        let result = await User.search({
            where: {
                age: Op.like("1_")
            }
        });

        expect(JSON.stringify(result)).toBe(JSON.stringify([
            {
                id: 3,
                name: "Alex",
                sex: "male",
                age: 19,
                weight: 65
            },
            {
                id: 4,
                name: "Luci",
                sex: "female",
                age: 16,
                weight: 45
            },
            {
                id: 5,
                name: "Oleg",
                sex: "male",
                age: 15,
                weight: 54
            }
        ]));
    });

    // it("should find John and Alex", async () =>
    // {
    //     let result = await User.search({
    //         where: {
    //             name: Op.iLike("jo%"),
    //             [Op.OR]: {
    //                 name: Op.iLike("al%")
    //             }
    //         }
    //     });
    //
    //     expect(JSON.stringify(result)).toBe(JSON.stringify([
    //         {
    //             id: 1,
    //             name: "John",
    //             sex: "male",
    //             age: 21,
    //             weight: 71
    //         },
    //         {
    //             id: 3,
    //             name: "Alex",
    //             sex: "male",
    //             age: 19,
    //             weight: 65
    //         }
    //     ]));
    // });

    it("should find ids 1,2,3", async () =>
    {
        let result = await User.search({
            where: {
                id: Op.in([1,2,3])
            }
        });

        expect(JSON.stringify(result)).toBe(JSON.stringify([
            {
                id: 1,
                name: "John",
                sex: "male",
                age: 21,
                weight: 71
            },
            {
                id: 2,
                name: "Marry",
                sex: "female",
                age: 25,
                weight: 56
            },
            {
                id: 3,
                name: "Alex",
                sex: "male",
                age: 19,
                weight: 65
            }
        ]));
    });

    it("should find age 15-16 y.o.", async () =>
    {
        let result = await User.search({
            where: {
                age: Op.and([Op.lte(16), Op.gte(15)])
            }
        });

        expect(JSON.stringify(result)).toBe(JSON.stringify([
            {
                id: 4,
                name: "Luci",
                sex: "female",
                age: 16,
                weight: 45
            },
            {
                id: 5,
                name: "Oleg",
                sex: "male",
                age: 15,
                weight: 54
            }
        ]));
    });

    it("should find all users except 1", async () =>
    {
        let result = await User.search({
            where: {
                id: Op.not(Op.eq(1))
            }
        });

        expect(JSON.stringify(result)).toBe(JSON.stringify([
            {
                id: 2,
                name: "Marry",
                sex: "female",
                age: 25,
                weight: 56
            },
            {
                id: 3,
                name: "Alex",
                sex: "male",
                age: 19,
                weight: 65
            },
            {
                id: 4,
                name: "Luci",
                sex: "female",
                age: 16,
                weight: 45
            },
            {
                id: 5,
                name: "Oleg",
                sex: "male",
                age: 15,
                weight: 54
            }
        ]));
    });

    it("should except sql injection by column", async () =>
    {
        const something: any = "\" DROP table; \"";
        let result = await User.search({
            where: {
                [something]: 1
            }
        });

        expect(JSON.stringify(result)).toBe(JSON.stringify([]));
    });

    it("should sort by age", async () =>
    {
        let result = await User.search({
            where: {},
            orderBy: {
                order: {column:"User.age", direction: "ASC"}
            }
        });

        console.log(result);

        expect(JSON.stringify(result)).toBe(JSON.stringify([
            {
                "id":5,
                "name":"Oleg",
                "sex":"male",
                "age":15,
                "weight":54
            },
            {
                "id":4,
                "name":"Luci",
                "sex":"female",
                "age":16,
                "weight":45
            },
            {
                "id":3,
                "name":"Alex",
                "sex":"male",
                "age":19,
                "weight":65
            },
            {
                "id":1,
                "name":"John",
                "sex":"male",
                "age":21,
                "weight":71
            },
            {
                "id":2,
                "name":"Marry",
                "sex":"female",
                "age":25,
                "weight":56
            }]));
    });
});




