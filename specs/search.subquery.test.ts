import { Connection, createConnection, getConnection } from "typeorm/index";
import { Product, Category } from "./models";
import { Op } from "../src";


describe("Subquery testing", () =>
{
    let conn: Connection;
    beforeAll(async () =>
    {
        conn = await createConnection({
            type: "sqlite",
            database: ":memory:",
            dropSchema: true,
            entities: [ Product, Category],
            synchronize: true,
            logging: false
        });

        const booksCategory = await Category.create({
            name: "Books"
        }).save();

        const newspapersCategory = await Category.create({
            name: "Newspapers"
        }).save();

        const magazinesCategory = await Category.create({
            name: "Magazines"
        }).save();

        await Product.insert({
            name: "Harry Potter",
            categoryId: booksCategory.id
        });

        await Product.insert({
            name: "Brave new world",
            categoryId: booksCategory.id
        });

        await Product.insert({
            name: "New York Times",
            categoryId: newspapersCategory.id
        });

        await Product.insert({
            name: "TJournal",
            categoryId: magazinesCategory.id
        })
    });

    afterAll(() => conn.close());

    it(`should find book with "H"`, async () =>
    {
        const result = await Product.search({
            where: {
                categoryId: Op.sub(Category, {
                    where: {
                        name: Op.like("%paper%")
                    }
                }, "id")
            }});


        expect(JSON.stringify(result)).toBe(JSON.stringify([{
            id: 3,
            name: 'New York Times',
            categoryId: 2
        }]));

    });
});
