import { SearchOperator } from "./SearchOperator"
import { BaseEntity, SelectQueryBuilder } from "typeorm/index";


export class Equal<T extends BaseEntity> extends SearchOperator
{
    constructor(public readonly value: unknown) {super();}

    public apply(qb: SelectQueryBuilder<T>, alias: string, property: string): void
    {
        const key = this.key(property);
        // TODO: проверить безопасность на SQL иньекции property
        qb.andWhere(`"${alias}"."${property}" = :${key}`, {[key]: this.value});
    }
}
