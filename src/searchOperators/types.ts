import { BaseEntity, ObjectType } from "typeorm/index";

export type ExtractKeys<T> = Extract<keyof T, string>;


export type Target<T extends BaseEntity> = {
    model: ObjectType<T>,
    column: ExtractKeys<T>
}