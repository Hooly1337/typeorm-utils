import { BaseEntity, SelectQueryBuilder } from "typeorm/index";
import { SearchOperator } from "./SearchOperator"


export class And<T extends BaseEntity> extends SearchOperator
{
    constructor(public readonly value: SearchOperator[]) {super();}

    public apply(qb: SelectQueryBuilder<T>, alias: string, property: string): void
    {
        for(const op of this.value)
            op.apply(qb, alias, property);
    }
}
