import { BaseEntity, SelectQueryBuilder } from "typeorm/index"


export abstract class SearchOperator
{
    private static prefCounter = 0;

    public static get pref()
    {
        if(this.prefCounter >= Number.MAX_SAFE_INTEGER)
            this.prefCounter = 0;
        return `pref_${(++this.prefCounter).toString(36)}_`;
    }

    public key(propKey: string): string
    {
        console.log(`${SearchOperator.pref}${propKey.replace(/"/g, '\\"')}`);
        return `${SearchOperator.pref}${propKey.replace(/"/g, '\\"')}`;
    }

    public abstract apply(
        qb: SelectQueryBuilder<any>,
        alias: string,
        property: string
    ): void
}