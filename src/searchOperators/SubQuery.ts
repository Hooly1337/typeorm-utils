import { SelectQueryBuilder, BaseEntity, ObjectType, WhereExpression } from "typeorm/index";
import { SearchOperator } from "./SearchOperator"
import { SearchOptions } from "../types";
import { Target } from "./types";
import { Model } from "../Model";


export class SubQuery<T extends BaseEntity> extends SearchOperator
{
    constructor(public readonly options: SearchOptions<T>, public readonly target: Target<T>) {super();}

    public apply(qb: SelectQueryBuilder<T>, alias: string, property: string): void
    {
        const {model, column} = this.target;
        const subAlias = model.name;
        const subQB = (model as typeof BaseEntity).createQueryBuilder(subAlias);
        // TODO: удостовериться что здесь не будет иньекции если подменить column:

        subQB.select([`"${subAlias}"."${column}"`]);
        subQB.limit(this.options.limit);

        Model.generateQuery(subQB, subAlias, this.options.where);
        const [subQuery, subParamsArray] = subQB.getQueryAndParameters();
        const namedSubParamsArray: Array<{key: string, value: any}> = subParamsArray.map((value, i) => ({
            key: this.key(`${i}`),
            value
        }));
        const subParams: Record<string, any> = namedSubParamsArray.reduce((memo, param) =>
        {
            memo[param.key] = param.value;
            return memo;
        }, {});
        console.log({subQuery, subParams});
        // TODO: проверить вставляет ли оно значения в query:
        qb.andWhere(`"${alias}"."${property}" IN (${
            subQuery.replace(/\?/g, () => 
            {
                const element = namedSubParamsArray.shift();
                if(!element)
                    throw new Error("Something went wrong. Too many '?' in query.");
                return `:${element.key}`;
            })
        })`, subParams);
    }
}
