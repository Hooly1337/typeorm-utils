import { SearchOperator } from "./SearchOperator"
import { BaseEntity, SelectQueryBuilder } from "typeorm/index";


export class ILike<T extends BaseEntity> extends SearchOperator
{
    constructor(public readonly value: string) {super();}

    public apply(qb: SelectQueryBuilder<T>, alias: string, property: string): void
    {
        const key = this.key(property);
        qb.andWhere(`"${alias}"."${property}" ILIKE :${key}`, {[key]: this.value});
    }
}
