import { SearchOperator } from "./SearchOperator"
import { SelectQueryBuilder } from "typeorm/index";


export class Like<T> extends SearchOperator
{
    constructor(public readonly value: string) {super();}

    public apply(qb: SelectQueryBuilder<T>, alias: string, property: string): void
    {
        const key = this.key(property);
        qb.andWhere(`${alias}.${property} LIKE :${key}`, {[key]: this.value});
    }
}
