import { BaseEntity, createQueryBuilder, ObjectType, SelectQueryBuilder } from "typeorm/index";
import { SearchOperator } from "./SearchOperator"
import { TWhere } from "../types";
import { Target } from "./types";

export class Not<T extends BaseEntity> extends SearchOperator
{
    constructor(public readonly searchOperator: SearchOperator) {super();}

    public apply(
        qb: SelectQueryBuilder<T>,
        alias: string,
        property: string
    ): void
    {
        const subQB = createQueryBuilder(alias);

        this.searchOperator.apply(subQB, alias, property);

        const subParamsArray = Object.values(subQB.getParameters());
        const subQuery = subQB.getSql().replace(/.* WHERE /s, "");
        const namedSubParamsArray: Array<{key: string, value: any}> = subParamsArray.map((value, i) => ({
            key: this.key(`${i}`),
            value
        }));
        const subParams: Record<string, any> = namedSubParamsArray.reduce((memo, param) =>
        {
            memo[param.key] = param.value;
            return memo;
        }, {});
        qb.andWhere(` NOT (${
            subQuery.replace(/\?/g, () =>
            {
                const element = namedSubParamsArray.shift();
                if(!element)
                    throw new Error("Something went wrong. Too many '?' in query.");
                return `:${element.key}`;
            })
            })`, subParams);
         // TODO: протести может оно вставляет сразу параметры.
        // TODO: то, что ты сделал используя condition.replace чревато sql иньекциями. за такое на бутылку сажают

    }
}
