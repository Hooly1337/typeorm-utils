import { BaseEntity, Brackets, ObjectType, SelectQueryBuilder, WhereExpression } from "typeorm/index";
import { SearchOperator } from "./searchOperators";
import { SearchOptions, SortOrder, TWhere } from "./types";
import { Op } from "./Op";



export class Model extends BaseEntity
{
    public static generateQuery<T extends Model>(
        this: ObjectType<T>,
        qb: SelectQueryBuilder<T>,
        alias: string,
        conditions: TWhere<T> | TWhere<T>[])
        : void
    {
        if (!(conditions instanceof Array))
            conditions = [conditions];

        if (conditions.length == 0)
            return;


        for (const condition of conditions)
        {
            console.log("condition", condition);
            if(Object.keys(condition).length == 0) // if no conditions
                continue;

            qb.orWhere(new Brackets((we: SelectQueryBuilder<T>) =>
            {
                for (const property in condition)
                {
                    let value = condition[property];

                    if (!(value instanceof SearchOperator))
                        value = Op.eq(value);

                    console.log({value});
                    if (value instanceof SearchOperator)
                        value.apply(we, alias, property);
                }
                if (condition[Op.OR])
                    (this as typeof Model).generateQuery(we, alias, condition[Op.OR]);
            }))
        }
    }

    public static async search<T extends Model>(this: ObjectType<T>, options: SearchOptions<T>): Promise<T[]>
    {
        const qb = (this as typeof Model).createQueryBuilder(this.name).select();
        (this as typeof Model).generateQuery(qb, this.name, options.where);

        if (options.limit)
            qb.limit(options.limit);

        if (options.orderBy) {
            let orders = options.orderBy.order;
            if (!(orders instanceof Array))
                orders = [orders];
            for(const order of orders) {
                qb.orderBy(order.column, order.direction)
            }
        }


        console.log("search > qb.getQueryAndParameters():", qb.getQueryAndParameters());
        return await qb.getMany() as T[];
    }

    public static async searchOne<T extends Model>(this: ObjectType<T>, options: SearchOptions<T>): Promise<T>
    {
        const qb = (this as typeof Model).createQueryBuilder(this.name).select();
        (this as typeof Model).generateQuery(qb, this.name, options.where);

        return await qb.getOneOrFail() as T;
    }
}
