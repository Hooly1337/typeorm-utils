import {
    And,
    Equal, ExtractKeys,
    GreaterThan,
    GreaterThanEqual, ILike,
    In,
    LessThan,
    LessThanEqual,
    Like, Not,
    SearchOperator, SubQuery
} from "./searchOperators";

import { SearchOptions } from "./types";
import { BaseEntity, ObjectType } from "typeorm/index";


export const Op = {
    eq<T extends BaseEntity>(value: unknown)
    {
        return new Equal<T>(value);
    },
    gt<T extends BaseEntity>(value: unknown)
    {
        return new GreaterThan<T>(value);
    },
    lt<T extends BaseEntity>(value: unknown)
    {
        return new LessThan<T>(value);
    },
    gte<T extends BaseEntity>(value: unknown)
    {
        return new GreaterThanEqual<T>(value);
    },
    lte<T extends BaseEntity>(value: unknown)
    {
        return new LessThanEqual<T>(value);
    },
    like<T extends BaseEntity>(value: string)
    {
        return new Like<T>(value);
    },
    iLike<T extends BaseEntity>(value: string)
    {
        return new ILike<T>(value);
    },
    in<T extends BaseEntity>(value: Array<unknown>)
    {
        return new In<T>(value);
    },
    and<T extends BaseEntity>(value: SearchOperator[])
    {
        return new And<T>(value);
    },
    not<T extends BaseEntity>(searchOperator: SearchOperator)
    {
         return new Not<T>(searchOperator, );
    },
    sub<T extends BaseEntity>(targetModel: ObjectType<T>, options: SearchOptions<T>, column: ExtractKeys<T>)
    {
        return new SubQuery<T>(options, {model: targetModel, column});
    },
    OR: Symbol("OR"),

};