import { SearchOperator } from "./searchOperators";
import { Op } from "./Op";
import { BaseEntity } from "typeorm/index";
import exp = require("constants");

export type TWhere<T extends BaseEntity> = {
    [P in keyof T]?: SearchOperator | T[P]
} & {
    [P in typeof Op.OR]?: TWhere<T> | TWhere<T>[]
};


export type SearchOptions<T extends BaseEntity> = {
    where: TWhere<T>,
    limit?: number,
    orderBy?: SortOptions
}

export type SortDirection = "ASC" | "DESC";
export type SortOrder = {column: string, direction: SortDirection};

export type SortOptions = {
    order: SortOrder | SortOrder[]
}